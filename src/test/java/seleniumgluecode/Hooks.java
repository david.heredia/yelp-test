package seleniumgluecode;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.chrome.ChromeDriver;

public class Hooks {

    private static ChromeDriver driver;
    private static int caseNum=0;


    @Before
    public void setUp(){
        caseNum++;

        System.setProperty("webdriver.chrome.driver","./src/test/resources/chromedriver/chromedriver");
        //Iniciando objeto driver:
        driver=new ChromeDriver();

        //Entrando a la página:
        driver.get("https://www.yelp.com/");

        //Maximizando la página:
        driver.manage().window().maximize();

        System.out.println("Initializing scenario "+ caseNum);

    }

    @After
    public void tearDown(){
        driver.quit();
    }

    public static ChromeDriver getDriver() {
        return driver;
    }
}
