package seleniumgluecode;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class Test extends TestBase{

    @Given("^that the user has entered Yelp$")
    public void that_the_user_has_entered_Yelp() throws Throwable {
        //Shh... do nothing!

    }

    @When("^the user searches for \"([^\"]*)\"$")
    public void the_user_searches_for(String word) throws Throwable {

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mainPage.simpleSearch(word);
    }

    @When("^appends the word \"([^\"]*)\" next to it, a report appears$")
    public void appends_the_word_next_to_it_a_report_appears(String additionalText) throws Throwable {

        //refine the search...
        mainPage.addToSearch(additionalText);

        //Open Report...

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //mainPage.report();

        mainPage.report();

    }

    @And("^chooses parameters \"([^\"]*)\" \\(\"([^\"]*)\"\\), and \"([^\"]*)\" \\(\"([^\"]*)\"\\)$")
    public void choosesParametersAnd(String header1, String option1, String header2, String option2) throws Throwable {

        mainPage.refineSearch(header1,option1,header2,option2);

    }

    @Then("^the reports would look a lot different$")
    public void the_reports_would_look_a_lot_different() throws Throwable {

        System.out.println("\n---Second Report---");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mainPage.report();

    }

    @Then("^the details for the first result will be seen$")
    public void the_details_for_the_first_result_will_be_seen() throws Throwable {
        mainPage.imFeelingLucky();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mainPage.captureComments();
    }


}
