package pom;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.List;

public class MainPage {
    public ChromeDriver driver;

    //"basic" XPath por the main page
    String defaultXPath="(//h3[text()=\"Sponsored Result\"]//..//../preceding-sibling::li//h4//ancestor::li[preceding-sibling::li//h3[text()=\"All Results\"]])";

    //default XPath for the details of certain item (address, phone, websites)
    String detailDefaultXPath="(//section[@class=\"lemon--section__373c0__fNwDM margin-b3__373c0__q1DuY border-color--default__373c0__3-ifU\"]//p)";

    //XPath for names in the comments of certain item
    String reviewerNameXPath="(//span[@class=\"lemon--span__373c0__3997G text__373c0__2Kxyz fs-block text-color--black-regular__373c0__2vGEn text-align--left__373c0__2XGa- text-weight--bold__373c0__1elNz text-size--large__373c0__3t60B\"])";

    String reviewXPath="(//span[@class=\"lemon--span__373c0__3997G text__373c0__2Kxyz fs-block text-color--black-regular__373c0__2vGEn text-align--left__373c0__2XGa- text-weight--bold__373c0__1elNz text-size--large__373c0__3t60B\"]//ancestor::li//p[@class=\"lemon--p__373c0__3Qnnj text__373c0__2Kxyz comment__373c0__1M-px text-color--normal__373c0__3xep9 text-align--left__373c0__2XGa-\"])";

    public void simpleSearch(String word){
        WebElement searchBox=driver.findElement(By.id("find_desc"));
        searchBox.click();

        WebElement suggestItem=driver.findElement(By.xpath("//li[@data-suggest-query='" +word+ "']"));
        suggestItem.click();
    }
    public void addToSearch(String additionalText){
        WebElement searchBox=driver.findElement(By.id("search_description"));
        searchBox.sendKeys(Keys.ARROW_RIGHT);
        searchBox.sendKeys(" "+additionalText);
        searchBox.sendKeys(Keys.ENTER);

    }
    public void refineSearch(String header1, String option1, String header2, String option2){
        String xBlock="//div[@class=' verticalLayout__09f24__A40CI display--inline-block__09f24__FsgS4 border-color--default__09f24__R1nRO']";

        //option 1...
        String xHead1=xBlock+"//p[text()=\""+header1+"\"]";

        String xOption1=xHead1+"//..//..//..//span[text()=\""+option1+"\"]";

        //option 2...
        String xHead2=xBlock+"//p[text()=\""+header2+"\"]";

        String xOption2=xHead2+"//..//..//..//span[text()=\""+option2+"\"]";

        //finding options 1 and 2, and clicking them
        WebElement linkOpt1=driver.findElement(By.xpath(xOption1));
        linkOpt1.click();

        WebElement linkOpt2=driver.findElement(By.xpath(xOption2));
        linkOpt2.click();
    }
    public void report(){

        int itemCount = driver.findElementsByXPath(defaultXPath).size();

        System.out.println("Item numbers: "+ itemCount);

        String itemName="";
        String itemRating="";


        for(int i=1;i<itemCount+1;i++){

            String itemXPath=defaultXPath+"["+i+"]//h4";

            String ratingXPath = defaultXPath + "["+i+"]//div[@role=\"img\"]";

            itemName=driver.findElement(By.xpath(itemXPath)).getText();

            if(driver.findElementsByXPath(ratingXPath).size()>0){
                itemRating=", "+driver.findElement(By.xpath(ratingXPath)).getAttribute("aria-label");
                System.out.println(itemName+itemRating);
            } else{
                System.out.println(itemName+", no rating");
            }

        }

    }

    public void imFeelingLucky(){

        //String itemsXPath=defaultXPath+"//span[@class=\" text__09f24__2tZKC text-color--black-regular__09f24__1QxyO text-align--left__09f24__3Drs0 text-weight--bold__09f24__WGVdT text-size--inherit__09f24__2rwpp\"]";

        String itemXPath=defaultXPath+"[1]//h4//a";

        WebElement selectedItem=driver.findElement(By.xpath(itemXPath));

        String itemName=selectedItem.getText();

        selectedItem.click();

        System.out.println("\n----The first result: "+itemName+"----");
    }

    public void captureComments(){

        //phone
        String phoneXPath=detailDefaultXPath+"[4]";

        String webXPath=detailDefaultXPath+"[2]//a";
        String addressXpath=detailDefaultXPath+"[5]/p";

        WebElement phoneField=driver.findElement(By.xpath(phoneXPath));
        System.out.println("Phone: "+phoneField.getText());
        //website

        WebElement webField=driver.findElement(By.xpath(webXPath));
        System.out.println("Website: "+webField.getText());

        //address

        WebElement addressField=driver.findElement(By.xpath(addressXpath));
        System.out.println("Address: "+addressField.getText());

        //first 3 reviews
        WebElement reviewerElement;
        WebElement commentElement;

        String thisReviewer="";
        String thisComment="";

        System.out.println("\nFirst 3 Reviews:");

        for (int i=1; i<4;i++){

            reviewerElement=driver.findElement(By.xpath(reviewerNameXPath+"["+i+"]"));
            thisReviewer=reviewerElement.getText();

            commentElement=driver.findElement(By.xpath(reviewXPath+"["+i+"]"));
            thisComment=commentElement.getText();

            System.out.println("\n"+thisReviewer+":");
            System.out.println(thisComment);


        }





    }
    public MainPage(ChromeDriver driver){
        this.driver=driver;
    }
}
