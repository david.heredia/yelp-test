Feature: A Little Search at Yelp
  As a user, i want to search for the best items on Yelp!

  Scenario: Default
    Given that the user has entered Yelp
    When the user searches for "Restaurants"
    And appends the word "pizza" next to it, a report appears
    And chooses parameters "Neighborhoods" ("Outer Sunset"), and "Distance" ("Bird's-eye View")
    Then the reports would look a lot different
    And the details for the first result will be seen

